class TestBugH3 {
    public static void main(String[] a) {
	       System.out.println(new Test().f());
    }
}

      //this test makes sure when the condition is false, the loop ends to prove this
      //and when the count reaches five then it exits. It will output 5 if correct.
class Test {

	   public int f() {
	     int count;
	     boolean done;
	     count = 0;
	     done = true;
	     do {
		          if(count == 5){
			               done = false;
		          }
		          else
		          {
			              count = count + 1;
		          }
	       } while (done == true);

	        return count;
       }

}
