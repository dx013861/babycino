class TestBugH2 {
    public static void main(String[] a) {
        System.out.println(new Test().f());
    }
}

      //this test tests if the expression is done before condition is checked.
      //This will output 1 if correct and will only run once as the condition is
      //met before the function is even ran.
class Test {

      public int f() {
          int count;
          boolean done;
          done = true;
          count = 0;
          do {
                count = count + 1;
              } while (count < 0);

              return count;
          }

}
